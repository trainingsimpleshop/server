'use strict';

const PAYMENTSTATUS = {
  CART: 'cart',
  PENDINGPAYMENT: 'pendingPayment',
  PENDINGREVIEW: 'pendingReview',
  PAID: 'paid',
  REJECT: 'reject'
}
module.exports = function(Order) {
  Order.prototype.recalculateTotalAmount = async function() {
    const orderItems = await this.orderItems.find({
      include: ['product'],
    })
    const itemPrices = orderItems.map(orderItem => orderItem.quantity * orderItem.toJSON().product.price)
    const totalPrice = itemPrices.reduce((total, current) => total + current, 0)
    this.totalAmount = totalPrice
    this.grandTotal = this.totalAmount + this.shippingCost
    return this.save()
  }

  /**
   * 
   * @param {Function(Error, object)} callback
   */

  Order.prototype.checkout = function() {
    this.status = PAYMENTSTATUS.PENDINGPAYMENT
    return this.save()
  };
  /**
   * 
   * @param {string} paymentReference 
   * @param {Function(Error, object)} callback
   */

  Order.prototype.verify = function(paymentReference) {
    this.paymentReference = paymentReference
    this.status = PAYMENTSTATUS.PENDINGREVIEW
    return this.save()
  };
  /**
   * 
   * @param {Function(Error, object)} callback
   */

  Order.prototype.approve = function() {
    this.status = PAYMENTSTATUS.PAID
    return this.save()
  };
  /**
   * 
   * @param {Function(Error, object)} callback
   */

  Order.prototype.reject = function() {
    this.status = PAYMENTSTATUS.REJECT
    return this.save()
  };
};
