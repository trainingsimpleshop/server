'use strict';

module.exports = function(Orderitem) {
  Orderitem.observe('after save', async (ctx) => {
    // recalculate order totalAmount
    const {
      instance
    } = ctx
    if (!instance) {
      return Promise.resolve()
    }
    const order = await instance.order.get()
    // recaluclate
    return order.recalculateTotalAmount()
  });
};
