'use strict';

module.exports = async function(server) {
  const {
    Container
  } = server.models
  try {
    await Container.getContainer('upload')
  } catch (e) {
    // don't have upload folder
    // create upload container
    await Container.createContainer({
      name: 'upload'
    })
  }

  return Promise.resolve()
};
