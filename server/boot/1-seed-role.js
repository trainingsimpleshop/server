'use strict';

module.exports = async function(server) {
  const {
    Role
  } = server.models
  try {
    const role = await Role.findOne({
      where: {
        name: 'admin'
      }
    })
    // create admin role if not exist
    if (!role) {
      // create role
      await Role.create({
        name: 'admin'
      })
      console.log('admin role created')
    }
  } catch (e) {
    console.log('cannot create role')
  }

  return Promise.resolve()
};
