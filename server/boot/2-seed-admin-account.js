'use strict';

module.exports = async function(server) {
  const {
    AppUser,
    Role,
    RoleMapping,
  } = server.models
  try {
    let adminUser = await AppUser.findOne({
      where: {
        username: 'administrator'
      }
    })
    // create admin role if not exist
    if (!adminUser) {
      // create role
      const createdUser = await AppUser.create({
        username: 'administrator',
        password: 'password',
        email: 'admin@yopmail.com',
        firstname: 'Admin',
        lastname: 'Training'
      })
      console.log('user created', createdUser)
      adminUser = createdUser
    }

    // check if admin account have admin role
    // get admin role
    const adminRole = await Role.findOne({
      where: {
        name: 'admin'
      }
    })
    const principals = await adminRole.principals.find({
      where: {
        principalType: RoleMapping.USER,
        principalId: adminUser.id
      }
    })
    if (principals.length === 0) {
      await adminRole.principals.create({
        principalType: RoleMapping.USER,
        principalId: adminUser.id
      })
      console.log('binding admin role to admin user')
    }
  } catch (e) {
    console.log('cannot create admin user')
  }

  return Promise.resolve()
};
